﻿
using UnityEngine;

public class DashAbility : MonoBehaviour, IAbility
{
    [SerializeField] private float _range = 0.1f;
    [SerializeField] private float _maxTimeToDash = 1.0f;

    public float MaxTimeToAbility { get => _maxTimeToDash; }



    public void Execute()
    {
        var pos = transform.position;
        pos += new Vector3(_range * Mathf.Sin(transform.eulerAngles.y * Mathf.Deg2Rad), 0, _range * Mathf.Cos(transform.eulerAngles.y * Mathf.Deg2Rad));
        transform.position = pos;
    }
}

