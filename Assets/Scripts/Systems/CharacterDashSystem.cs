﻿using Unity.Entities;
using UnityEngine;

public class CharacterDashSystem : ComponentSystem
{
    private EntityQuery _dashQuery;
    private float _timeToAbility = 0;

    protected override void OnCreate()
    {
        _dashQuery = GetEntityQuery(ComponentType.ReadOnly<InputData>(),
            ComponentType.ReadOnly<DashData>(),
            ComponentType.ReadOnly<CharacterConvertToEntity>(),
            ComponentType.ReadOnly<Transform>());
    }

    protected override void OnUpdate()
    {
        Entities.With(_dashQuery).ForEach(
            (Entity entity, CharacterConvertToEntity character, ref InputData inputData) =>
            {
                if (character.dashAction != null && character.dashAction is IAbility ability)
                {
                    if (_timeToAbility <= 0)
                    {
                        if (inputData.dash > 0)
                        {
                            ability.Execute();
                            _timeToAbility = ability.MaxTimeToAbility;
                        }
                    }
                    else
                    {
                        _timeToAbility -= Time.DeltaTime;
                    }
                }
            });
    }
}
