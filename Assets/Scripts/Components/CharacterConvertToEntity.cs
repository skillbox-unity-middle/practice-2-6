using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class CharacterConvertToEntity : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField] private float2 _speed = new float2(1.0f, 1.0f);

    public MonoBehaviour dashAction;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity, new InputData());
        dstManager.AddComponentData(entity, new MoveData { 
            speed = _speed
        });
        if(dashAction != null && dashAction is IAbility)
        {
            dstManager.AddComponentData(entity, new DashData());
        }
    }
}
