﻿public interface IAbility
{
    public float MaxTimeToAbility { get; }

    public void Execute();
}

